                                    Assignment 4:

1) What will be output of following ? 
typeof (function* f() { yield f })().next().next()
Output: typeof.PNG
    

2)Write a program to print Fibonacci series of numbers using generators ?
Output: fibo.PNG

PROBLEM STATEMENT
3) Raghu is the logistics manager in a paper company. His company produces paper and sells them in different countries. As different countries have different sales tax, the price of paper varies in each country. Raghu is tasked to calculate the price of all products of his company in different countries.
 Write a program, which Raghu can use to generate the final price of different products
  Output: salestax.html
  
                                    Assignment 3:

1) Handle all types of error in js [Range, Reference, Syntax, Type ,URI Error, Custom exception]
Output: error.html

3) Make a form using jquery which will contain a textbox and button. on clicking the button will add the text to the list and on remove just remove the element.
Output: jquery.html

                                    Assignment 2:

1) To create a array of products with name, quantity and unit price and calculate the totalprice of products and also give user chance to update the quantity and display the totalprice.
Output: Store.html

                                    Assignment 1:

1) Write a function which returns elements of an array after 3 seconds
Output: First.html

2) Write a function which sorts the products on the basis of the given id. Create array of products with keys Product name , Product Id,  Product manufacture date, Product expiry date, is available today.
Output: Second.html

3) Create a search filter for products.
Output: Third.html

4) Return all products which are available today.
Output: Fourth.html



